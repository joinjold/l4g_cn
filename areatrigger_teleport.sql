update areatrigger_teleport set name='血色修道院西(墓地)副本起点' where id=45;
update areatrigger_teleport set name='死亡矿井(副本)A-西部荒野' where id=78;
update areatrigger_teleport set name='暴风城监狱-(副本)入口' where id=101;
update areatrigger_teleport set name='暴风城地窑' where id=107;
update areatrigger_teleport set name='暴风城地窑' where id=109;
update areatrigger_teleport set name='死亡矿井A门口-西部荒野' where id=119;
update areatrigger_teleport set name='死亡矿井B出口-西部荒野' where id=121;
update areatrigger_teleport set name='影牙城堡(副本)-银松森林' where id=145;
update areatrigger_teleport set name='影牙城堡 -入口' where id=194;
update areatrigger_teleport set name='哀嚎洞穴门口-贫瘠之地' where id=226;
update areatrigger_teleport set name='哀嚎洞穴(副本)-贫瘠之地' where id=228;
update areatrigger_teleport set name='剃刀沼泽门口-贫瘠之地' where id=242;
update areatrigger_teleport set name='剃刀沼泽(副本)-贫瘠之地' where id=244;
update areatrigger_teleport set name='黑暗深渊(副本)-灰谷' where id=257;
update areatrigger_teleport set name='黑暗深渊门口-灰谷' where id=259;
update areatrigger_teleport set name='奥达曼(副本)A-荒芜之地' where id=286;
update areatrigger_teleport set name='奥达曼门口' where id=288;
update areatrigger_teleport set name='诺莫瑞根门口A-丹莫罗' where id=322;
update areatrigger_teleport set name='诺莫瑞根(副本)A-丹莫罗' where id=324;
update areatrigger_teleport set name='剃刀高地(副本)-贫瘠之地' where id=442;
update areatrigger_teleport set name='剃刀高地门口-贫瘠之地' where id=444;
update areatrigger_teleport set name='阿塔哈卡神庙(副本)-悲伤沼泽' where id=446;
update areatrigger_teleport set name='阿塔哈卡神庙门口-悲伤沼泽' where id=448;
update areatrigger_teleport set name='暴风城监狱-门口' where id=503;
update areatrigger_teleport set name='诺莫瑞根(副本)B-丹莫罗' where id=523;
update areatrigger_teleport set name='诺莫瑞根门口B-丹莫罗' where id=525;
update areatrigger_teleport set name='泰达希尔-鲁瑟兰村' where id=527;
update areatrigger_teleport set name='泰达希尔-达纳苏斯' where id=542;
update areatrigger_teleport set name='血色修道院西(墓地)门口' where id=602;
update areatrigger_teleport set name='血色修道院西(大教堂)门口' where id=604;
update areatrigger_teleport set name='血色修道院东(军械库)门口' where id=606;
update areatrigger_teleport set name='血色修道院东(图书馆)门口' where id=608;
update areatrigger_teleport set name='血色修道院西(大教堂)副本起点' where id=610;
update areatrigger_teleport set name='血色修道院东(军械库)副本起点' where id=612;
update areatrigger_teleport set name='血色修道院东(图书馆)副本起点' where id=614;
update areatrigger_teleport set name='暴风城-巫师圣殿' where id=702;
update areatrigger_teleport set name='暴风城-巫师圣殿入口' where id=704;
update areatrigger_teleport set name='奥达曼门口B-荒芜之地' where id=882;
update areatrigger_teleport set name='奥达曼(副本)B-荒芜之地' where id=902;
update areatrigger_teleport set name='祖尔法拉克门口-塔纳利斯' where id=922;
update areatrigger_teleport set name='祖尔法拉克(副本)-塔纳利斯' where id=924;
update areatrigger_teleport set name='信仰的飞跃 - 秋季结束' where id=943;
update areatrigger_teleport set name='奥妮克希亚的巢穴门口-尘泥沼泽' where id=1064;
update areatrigger_teleport set name='黑石深渊(副本)-黑石山' where id=1466;
update areatrigger_teleport set name='黑石塔-灼热峡谷黑手大厅' where id=1468;
update areatrigger_teleport set name='黑石塔-灼热峡谷熔岩之桥' where id=1470;
update areatrigger_teleport set name='黑石深渊门口-黑石山' where id=1472;
update areatrigger_teleport set name='矿道地铁-铁炉堡' where id=2166;
update areatrigger_teleport set name='矿道地铁-暴风城' where id=2171;
update areatrigger_teleport set name='矿道地铁-暴风城' where id=2173;
update areatrigger_teleport set name='矿道地铁-铁炉堡' where id=2175;
update areatrigger_teleport set name='斯坦索姆(副本)B-东瘟疫之地' where id=2214;
update areatrigger_teleport set name='斯坦索姆-东瘟疫之地出口A2' where id=2216;
update areatrigger_teleport set name='斯坦索姆-东瘟疫之地出口A1' where id=2217;
update areatrigger_teleport set name='斯坦索姆门口B-东瘟疫之地' where id=2221;
update areatrigger_teleport set name='怒焰裂谷门口-奥格瑞玛' where id=2226;
update areatrigger_teleport set name='怒焰裂谷(副本)-奥格瑞玛' where id=2230;
update areatrigger_teleport set name='奥格瑞玛-传说大厅' where id=2527;
update areatrigger_teleport set name='奥格瑞玛-(勇者大厅)入口' where id=2530;
update areatrigger_teleport set name='暴风城-冠军大厅' where id=2532;
update areatrigger_teleport set name='暴风城-(勇士大厅)入口' where id=2534;
update areatrigger_teleport set name='通灵学院(副本)-西瘟疫之地' where id=2567;
update areatrigger_teleport set name='通灵学院门口-西瘟疫之地' where id=2568;
update areatrigger_teleport set name='PVP1奥托兰克山谷-部落入口' where id=2606;
update areatrigger_teleport set name='PVP1奥托兰克山谷-联盟入口' where id=2608;
update areatrigger_teleport set name='奥妮克希亚的巢穴(副本)-尘泥沼泽' where id=2848;
update areatrigger_teleport set name='熔火之桥' where id=2886;
update areatrigger_teleport set name='熔火之心 - 入口, 里面' where id=2890;
update areatrigger_teleport set name='迈拉顿门口B-凄凉之地' where id=3126;
update areatrigger_teleport set name='迈拉顿门口A-凄凉之地' where id=3131;
update areatrigger_teleport set name='迈拉顿(副本)A-凄凉之地' where id=3133;
update areatrigger_teleport set name='迈拉顿(副本)B-凄凉之地' where id=3134;
update areatrigger_teleport set name='厄运之槌东A(副本)-菲拉斯' where id=3183;
update areatrigger_teleport set name='厄运之槌东B(副本)-菲拉斯' where id=3184;
update areatrigger_teleport set name='厄运之槌东C(副本)-菲拉斯' where id=3185;
update areatrigger_teleport set name='厄运之槌西B(副本)-菲拉斯' where id=3186;
update areatrigger_teleport set name='厄运之槌西A(副本)-菲拉斯' where id=3187;
update areatrigger_teleport set name='厄运之槌北(副本)-菲拉斯' where id=3189;
update areatrigger_teleport set name='厄运之槌西B门口-菲拉斯' where id=3190;
update areatrigger_teleport set name='厄运之槌西A门口-菲拉斯' where id=3191;
update areatrigger_teleport set name='厄运之槌北门口-菲拉斯' where id=3193;
update areatrigger_teleport set name='厄运之槌东A门口-菲拉斯' where id=3194;
update areatrigger_teleport set name='厄运之槌东B门口-菲拉斯' where id=3195;
update areatrigger_teleport set name='厄运之槌东C门口-菲拉斯' where id=3196;
update areatrigger_teleport set name='厄运之槌东D门口-菲拉斯' where id=3197;
update areatrigger_teleport set name='熔火之心之窗 入口' where id=3528;
update areatrigger_teleport set name='熔火之心之窗 (熔岩) 入口' where id=3529;
update areatrigger_teleport set name='黑翼之巢入口-灼热峡谷-燃烧平原-东部王国' where id=3726;
update areatrigger_teleport set name='黑翼之巢出口-黑石山-东部王国' where id=3728;
update areatrigger_teleport set name='祖尔格拉布(副本)-荆棘谷' where id=3928;
update areatrigger_teleport set name='祖尔格拉布门口-荆棘谷' where id=3930;
update areatrigger_teleport set name='PVP4阿拉希盆地联盟出口' where id=3948;
update areatrigger_teleport set name='PVP4落锤镇部落入口' where id=3949;
update areatrigger_teleport set name='安其拉废墟门口-希利苏斯' where id=4006;
update areatrigger_teleport set name='安其拉废墟(副本)-希利苏斯' where id=4008;
update areatrigger_teleport set name='安其拉神殿(副本)-希利苏斯' where id=4010;
update areatrigger_teleport set name='安其拉神殿门口-希利苏斯' where id=4012;
update areatrigger_teleport set name='纳克萨玛斯 (出口)' where id=4055;
update areatrigger_teleport set name='卡拉赞门楼(入口处)' where id=4131;
update areatrigger_teleport set name='卡拉赞上层' where id=4135;
update areatrigger_teleport set name='碎裂大厅出口' where id=4145;
update areatrigger_teleport set name='蒸汽洞窟出口' where id=4147;
update areatrigger_teleport set name='玛瑟里顿的巢穴出口' where id=4149;
update areatrigger_teleport set name='地狱火壁垒' where id=4150;
update areatrigger_teleport set name='破碎大厅' where id=4151;
update areatrigger_teleport set name='蒸汽洞窟' where id=4152;
update areatrigger_teleport set name='玛瑟里顿的巢穴' where id=4153;
update areatrigger_teleport set name='纳克萨玛斯(副本出口)' where id=4156;
update areatrigger_teleport set name='地狱火壁垒出口' where id=4297;
update areatrigger_teleport set name='到海家尔人类营地之门' where id=4311;
update areatrigger_teleport set name='从海家尔到部落营地之门' where id=4312;
update areatrigger_teleport set name='从海家尔到暗夜精灵村庄生命之树之门' where id=4313;
update areatrigger_teleport set name='海家尔山峰入口' where id=4319;
update areatrigger_teleport set name='黑色沼泽出口' where id=4320;
update areatrigger_teleport set name='时光之穴出口' where id=4321;
update areatrigger_teleport set name='黑色沼泽出口' where id=4322;
update areatrigger_teleport set name='从海家尔到CoT 主门' where id=4323;
update areatrigger_teleport set name='时光之穴入口' where id=4324;
update areatrigger_teleport set name='外域到黑暗之门' where id=4352;
update areatrigger_teleport set name='黑暗之门到外域' where id=4354;
update areatrigger_teleport set name='沼泽洞穴入口' where id=4363;
update areatrigger_teleport set name='蒸汽洞窟入口' where id=4364;
update areatrigger_teleport set name='奴隶围栏入口' where id=4365;
update areatrigger_teleport set name='蒸汽洞窟出口' where id=4366;
update areatrigger_teleport set name='沼泽洞穴出口' where id=4367;
update areatrigger_teleport set name='奴隶围栏出口' where id=4379;
update areatrigger_teleport set name='逐日岛到东瘟疫之地' where id=4386;
update areatrigger_teleport set name='暗影迷宫出口' where id=4397;
update areatrigger_teleport set name='塞泰克大厅出口' where id=4399;
update areatrigger_teleport set name='法力坟墓出口' where id=4401;
update areatrigger_teleport set name='奥金顿墓穴出口' where id=4403;
update areatrigger_teleport set name='奥金顿墓穴入口' where id=4404;
update areatrigger_teleport set name='法力坟墓入口' where id=4405;
update areatrigger_teleport set name='塞泰克大厅入口' where id=4406;
update areatrigger_teleport set name='暗影迷宫入口' where id=4407;
update areatrigger_teleport set name='东瘟疫之地到逐日岛' where id=4409;
update areatrigger_teleport set name='盘牙洞穴入口' where id=4416;
update areatrigger_teleport set name='盘牙洞穴出口' where id=4418;
update areatrigger_teleport set name='卡拉赞门房 (出口)' where id=4436;
update areatrigger_teleport set name='拱廊区出口' where id=4455;
update areatrigger_teleport set name='凤凰大厅出口' where id=4457;
update areatrigger_teleport set name='秘术区出口' where id=4459;
update areatrigger_teleport set name='机械区出口' where id=4461;
update areatrigger_teleport set name='秘术区入口' where id=4467;
update areatrigger_teleport set name='拱廊区入口' where id=4468;
update areatrigger_teleport set name='机械区入口' where id=4469;
update areatrigger_teleport set name='凤凰大厅入口' where id=4470;
update areatrigger_teleport set name='海家尔峰出口' where id=4487;
update areatrigger_teleport set name='卡拉赞, 广场 (出口)' where id=4520;
update areatrigger_teleport set name='索克雷萨出口' where id=4523;
update areatrigger_teleport set name='格鲁尔巢穴出口' where id=4534;
update areatrigger_teleport set name='格鲁尔巢穴入口' where id=4535;
update areatrigger_teleport set name='侵略点:灾难 (回归)' where id=4561;
update areatrigger_teleport set name='侵略点:灾难 (回归)' where id=4562;
update areatrigger_teleport set name='黑暗神庙' where id=4598;
update areatrigger_teleport set name='波塔尼卡 - 麦克纳尔' where id=4612;
update areatrigger_teleport set name='麦克纳尔 - 波塔尼卡' where id=4614;
update areatrigger_teleport set name='黑暗神庙出口' where id=4619;
update areatrigger_teleport set name='祖阿曼入口' where id=4738;
update areatrigger_teleport set name='祖阿曼出口' where id=4739;
update areatrigger_teleport set name='魔班主任平台出口' where id=4885;
update areatrigger_teleport set name='魔班主任平台入口' where id=4887;
update areatrigger_teleport set name='太阳之井高地入口' where id=4889;
update areatrigger_teleport set name='太阳之井高地出口' where id=4891;
